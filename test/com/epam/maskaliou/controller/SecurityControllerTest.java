package com.epam.maskaliou.controller;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.servlet.view.AbstractUrlBasedView;
import org.springframework.web.servlet.view.InternalResourceView;
import org.springframework.web.servlet.view.InternalResourceViewResolver;

public class SecurityControllerTest
{
    @InjectMocks
    private SecurityController securityController;

    private MockMvc mockMvc;

    @Before
    public void init()
    {
	MockitoAnnotations.initMocks(this);
	mockMvc = MockMvcBuilders.standaloneSetup(securityController)
		.setViewResolvers(new StandaloneMvcTestViewResolver()).build();
    }

    @Test
    public void testLogin() throws Exception
    {
	mockMvc.perform(get("/login")).andExpect(status().isOk()).andExpect(forwardedUrl("login"));
    }

    @Test
    public void testAcessDenied() throws Exception
    {
	mockMvc.perform(get("/accessDenied")).andExpect(status().isOk()).andExpect(forwardedUrl("accessDenied"));
    }
    
    @Test
    public void testLogout() throws Exception
    {
	mockMvc.perform(get("/logout")).andExpect(status().is3xxRedirection());
    }

    /**
     * For preventing errors with "circular view path".
     */
    class StandaloneMvcTestViewResolver extends InternalResourceViewResolver
    {
	public StandaloneMvcTestViewResolver()
	{
	    super();
	}

	@Override
	protected AbstractUrlBasedView buildView(final String viewName) throws Exception
	{
	    final InternalResourceView view = (InternalResourceView) super.buildView(viewName);
	    view.setPreventDispatchLoop(false);
	    return view;
	}
    }
}
