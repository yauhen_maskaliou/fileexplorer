package com.epam.maskaliou.controller;

import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import com.epam.maskaliou.entity.MutualList;
import com.epam.maskaliou.service.IFileService;

public class FileControllerTest
{
    private static final String DISK = "D:/";

    @InjectMocks
    private FileController fileController;

    @Mock
    private IFileService fileService;

    private MockMvc mockMvc;

    @Before
    public void init()
    {
	MockitoAnnotations.initMocks(this);
	mockMvc = MockMvcBuilders.standaloneSetup(fileController).build();
    }

    @Test
    public void testStart() throws Exception
    {
	when(fileService.getFromFolder(DISK)).thenReturn(new MutualList());

	mockMvc.perform(get("/")).andExpect(status().isOk()).andExpect(forwardedUrl("explorer"))
		.andExpect(model().attribute("mutualList", new MutualList()));

	verify(fileService, times(1)).getFromFolder(DISK);
	verifyNoMoreInteractions(fileService);
    }

    @Test
    public void testShowFolderContent() throws Exception
    {
	when(fileService.getFromFolder(DISK + "folder/name/")).thenReturn(new MutualList());

	mockMvc.perform(get("/viewFolder").param("folderName", "name").param("currentFolder", "folder"))
		.andExpect(status().isOk()).andExpect(forwardedUrl("explorer"))
		.andExpect(model().attribute("mutualList", new MutualList()));

	verify(fileService, times(1)).getFromFolder(DISK + "folder/name/");
	verifyNoMoreInteractions(fileService);
    }

    @Test
    public void testShowFile() throws Exception
    {
	when(fileService.getFileText(DISK + "folder/name")).thenReturn("text");

	mockMvc.perform(get("/viewFile").param("fileName", "name").param("currentFolder", "folder"))
		.andExpect(status().isOk()).andExpect(forwardedUrl("file"))
		.andExpect(model().attribute("fileText", "text"));

	verify(fileService, times(1)).getFileText(DISK + "folder/name");
	verifyNoMoreInteractions(fileService);
    }

    @Test
    public void testBack() throws Exception
    {
	when(fileService.getFromFolder(DISK)).thenReturn(new MutualList());

	mockMvc.perform(get("/back").param("currentFolder", "")).andExpect(status().isOk())
		.andExpect(forwardedUrl("explorer")).andExpect(model().attribute("mutualList", new MutualList()));

	verify(fileService, times(1)).getFromFolder(DISK);
	verifyNoMoreInteractions(fileService);
    }

    @Test
    public void testBack2() throws Exception
    {
	when(fileService.getFromFolder(DISK + "folder/")).thenReturn(new MutualList());

	mockMvc.perform(get("/back").param("currentFolder", "folder//")).andExpect(status().isOk())
		.andExpect(forwardedUrl("explorer")).andExpect(model().attribute("mutualList", new MutualList()));

	verify(fileService, times(1)).getFromFolder(DISK + "folder/");
	verifyNoMoreInteractions(fileService);
    }
}
