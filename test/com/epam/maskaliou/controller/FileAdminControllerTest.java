package com.epam.maskaliou.controller;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.forwardedUrl;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.model;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import com.epam.maskaliou.entity.MutualList;
import com.epam.maskaliou.service.IFileService;

public class FileAdminControllerTest
{
    private static final String DISK = "D:/";

    @InjectMocks
    private FileAdminController fileAdminController;

    @Mock
    private IFileService fileService;

    private MockMvc mockMvc;

    @Before
    public void init()
    {
	MockitoAnnotations.initMocks(this);
	mockMvc = MockMvcBuilders.standaloneSetup(fileAdminController).build();
    }

    @Test
    public void testNewFile() throws Exception
    {
	mockMvc.perform(get("/admin/newFile").param("currentFolder", "folder")).andExpect(status().isOk())
		.andExpect(forwardedUrl("newfile"));
    }

    @Test
    public void addFile() throws Exception
    {
	when(fileService.getFromFolder(DISK + "folder")).thenReturn(new MutualList());

	mockMvc.perform(get("/admin/addFile").param("fileName", "name").param("currentFolder", "folder//")
		.param("fileText", "text")).andExpect(status().isOk()).andExpect(forwardedUrl("explorer"))
		.andExpect(model().attribute("mutualList", new MutualList()));

	verify(fileService, times(1)).getFromFolder(DISK + "folder");
	verify(fileService, times(1)).addNewFile(DISK + "folder/name", "text");
	verifyNoMoreInteractions(fileService);
    }
    
    @Test
    public void addFolder() throws Exception
    {
	when(fileService.getFromFolder(DISK + "folder")).thenReturn(new MutualList());

	mockMvc.perform(get("/admin/addFolder").param("folderName", "name").param("currentFolder", "folder"))
		.andExpect(status().isOk()).andExpect(forwardedUrl("explorer"))
		.andExpect(model().attribute("mutualList", new MutualList()));

	verify(fileService, times(1)).getFromFolder(DISK + "folder");
	verify(fileService, times(1)).addNewFolder(DISK + "folder/name");
	verifyNoMoreInteractions(fileService);
    }

    @Test
    public void delete() throws Exception
    {
	when(fileService.getFromFolder(DISK + "folder")).thenReturn(new MutualList());

	mockMvc.perform(get("/admin/delete").param("fileName", "name").param("currentFolder", "folder"))
		.andExpect(status().isOk()).andExpect(forwardedUrl("explorer"))
		.andExpect(model().attribute("mutualList", new MutualList()));

	verify(fileService, times(1)).getFromFolder(DISK + "folder");
	verify(fileService, times(1)).deleteFile(DISK + "folder/name");
	verifyNoMoreInteractions(fileService);
    }
}
