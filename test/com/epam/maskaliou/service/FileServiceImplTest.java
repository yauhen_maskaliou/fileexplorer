package com.epam.maskaliou.service;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import com.epam.maskaliou.dao.IFileDAO;
import com.epam.maskaliou.entity.MutualList;
import com.epam.maskaliou.service.impl.FileServiceImpl;

public class FileServiceImplTest
{
    @Mock
    private IFileDAO fileDAO;
    
    private IFileService fileService;
    
    @Before
    public void init()
    {
	MockitoAnnotations.initMocks(this);
	fileService = new FileServiceImpl(fileDAO);
    }
    
    @Test
    public void testGetFromFolder() throws Exception
    {
	when(fileDAO.getFromFolder("URL")).thenReturn(new MutualList());
	
	fileService.getFromFolder("URL");
	verify(fileDAO, times(1)).getFromFolder("URL");
	assertEquals(fileDAO.getFromFolder("URL"), new MutualList());
    }
    
    @Test
    public void testGetFiletext() throws Exception
    {
	when(fileDAO.getFileText("URL")).thenReturn("file text");
	
	fileService.getFileText("URL");
	verify(fileDAO, times(1)).getFileText("URL");
	assertEquals(fileDAO.getFileText("URL"), "file text");
    }
    
    @Test
    public void testAddNewFolder() throws Exception
    {
	fileService.addNewFolder("path");
	verify(fileDAO, times(1)).addNewFolder("path");
    }
    
    @Test
    public void testAddNewFile() throws Exception
    {
	fileService.addNewFile("URL", "text");
	verify(fileDAO, times(1)).addNewFile("URL", "text");
    }
    
    @Test
    public void testDeleteFile() throws Exception
    {
	fileService.deleteFile("path");
	verify(fileDAO, times(1)).deleteFile("path");
    }
}
