jQuery(function($)
{
	var items;
	var perPage = 6;

	var $pagination = $("#pagination");
	$pagination.pagination(
	{
		itemsOnPage : perPage,
		cssStyle : "light-theme",
		onPageClick : function(pageNumber)
		{
			var showFrom = perPage * (pageNumber - 1);
			var showTo = showFrom + perPage;
			items.hide().slice(showFrom, showTo).show();
		},
		prevText : "<",
		nextText : ">"
	});

	function updateItems()
	{
		items = $("#files tbody tr");
	
		$pagination.pagination("updateItems", items.length);
	
		var page = Math.min($pagination.pagination("getCurrentPage"), $pagination.pagination("getPagesCount"));
		$pagination.pagination("selectPage", page);
	}
	
	updateItems();
});