package com.epam.maskaliou.controller;

import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.epam.maskaliou.service.IFileService;

@Controller
public class FileController
{
    public static final Logger LOG = Logger.getRootLogger();

    private static final String EXPLORER = "explorer";
    private static final String FILE = "file";
    private static final String ERROR = "error";
    private static final String DISK = "D:/";

    @Autowired
    private IFileService fileService;

    @RequestMapping(value = "/", method = RequestMethod.GET)
    public String start(ModelMap model, HttpSession session)
    {
	try
	{
	    model.addAttribute("mutualList", fileService.getFromFolder(DISK));
	    session.setAttribute("currentUser", FileUtils.getPrincipal());
	    model.addAttribute("currentURL", "");
	    return EXPLORER;
	}
	catch (Exception e)
	{
	    LOG.error(e.getMessage());
	    model.addAttribute("errorMessage", e.getMessage());
	    return ERROR;
	}
    }

    @RequestMapping(value = "/viewFolder", method = RequestMethod.GET)
    public String showFolderContent(@RequestParam("folderName") String folderName, @RequestParam("currentFolder") String currentURL,  ModelMap model, HttpSession session)
    {
	try
	{
	    String fullPath = DISK + currentURL + "/" + folderName + "/";

	    model.addAttribute("mutualList", fileService.getFromFolder(fullPath));
	    model.addAttribute("currentURL", currentURL + "/" + folderName + "/");
	    return EXPLORER;
	}
	catch (Exception e)
	{
	    LOG.error(e.getMessage());
	    model.addAttribute("errorMessage", e.getMessage());
	    return ERROR;
	}
    }

    @RequestMapping(value = "/viewFile", method = RequestMethod.GET)
    public String showFile(@RequestParam("fileName") String fileName, @RequestParam("currentFolder") String currentURL, ModelMap model, HttpSession session)
    {
	try
	{
	    String fullPath = DISK + currentURL + "/" + fileName;
	    
	    model.addAttribute("currentURL", currentURL + "/" + fileName + "/");
	    model.addAttribute("fileName", fileName);
	    model.addAttribute("fileText", fileService.getFileText(fullPath));
	    return FILE;
	}
	catch (Exception e)
	{
	    LOG.error(e.getMessage());
	    model.addAttribute("errorMessage", e.getMessage());
	    return ERROR;
	}
    }

    @RequestMapping(value = "/back", method = RequestMethod.GET)
    public String back(@RequestParam("currentFolder") String currentURL, ModelMap model, HttpSession session)
    {
	try
	{
	    if ("".equals(currentURL))
	    {
		model.addAttribute("mutualList", fileService.getFromFolder(DISK));
		return EXPLORER;
	    }
	    currentURL = currentURL.substring(0, currentURL.lastIndexOf('/'));
	    currentURL = currentURL.substring(0, currentURL.lastIndexOf('/'));
	    String fullPath = DISK + currentURL + "/";

	    model.addAttribute("mutualList", fileService.getFromFolder(fullPath));
	    model.addAttribute("currentURL", currentURL);
	    return EXPLORER;
	}
	catch (Exception e)
	{
	    LOG.error(e.getMessage());
	    model.addAttribute("errorMessage", e.getMessage());
	    return ERROR;
	}
    }

    public static class FileUtils
    {
	public static String getPrincipal()
	{
	    try
	    {
		String userName = null;
		Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();

		if (principal instanceof UserDetails)
		{
		    userName = ((UserDetails) principal).getUsername();
		}
		else
		{
		    userName = principal.toString();
		}

		return userName;
	    }
	    catch(Exception e)
	    {
		return "";
	    }
	}
    }
}