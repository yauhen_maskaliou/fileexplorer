package com.epam.maskaliou.controller;

import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.epam.maskaliou.service.IFileService;

@Controller
@RequestMapping("/admin")
public class FileAdminController
{
    public static final Logger LOG = Logger.getRootLogger();
    private static final String NEWFILE = "newfile";
    private static final String ERROR = "error";
    private static final String EXPLORER = "explorer";
    private static final String DISK = "D:/";

    @Autowired
    private IFileService fileService;

    @RequestMapping(value = "/newFile", method = RequestMethod.GET)
    public String newFile(@RequestParam("currentFolder") String currentURL, ModelMap model, HttpSession session)
    {
	try
	{
	    model.addAttribute("currentURL", currentURL + "/" + NEWFILE + "/");
	    return NEWFILE;
	}
	catch (Exception e)
	{
	    LOG.error(e);
	    model.addAttribute("errorMessage", e.getMessage());
	    return ERROR;
	}
    }

    @RequestMapping(value = "/addFile", method = RequestMethod.GET)
    public String addFile(@RequestParam("fileName") String fileName, @RequestParam("fileText") String fileText,
	    @RequestParam("currentFolder") String currentURL, ModelMap model, HttpSession session)
    {
	try
	{
	    currentURL = currentURL.substring(0, currentURL.lastIndexOf('/'));
	    currentURL = currentURL.substring(0, currentURL.lastIndexOf('/'));

	    String fullPath = DISK + currentURL + "/" + fileName;

	    fileService.addNewFile(fullPath, fileText);

	    model.addAttribute("mutualList", fileService.getFromFolder(DISK + currentURL));
	    model.addAttribute("currentURL", currentURL);
	    return EXPLORER;
	}
	catch (Exception e)
	{
	    LOG.error(e);
	    model.addAttribute("errorMessage", e.getMessage());
	    return ERROR;
	}
    }

    @RequestMapping(value = "/addFolder", method = RequestMethod.GET)
    public String addFolder(@RequestParam("folderName") String folderName,
	    @RequestParam("currentFolder") String currentURL, ModelMap model, HttpSession session)
    {
	try
	{
	    String fullPath = DISK + currentURL + "/" + folderName;

	    fileService.addNewFolder(fullPath);

	    model.addAttribute("mutualList", fileService.getFromFolder(DISK + currentURL));
	    model.addAttribute("currentURL", currentURL);
	    return EXPLORER;
	}
	catch (Exception e)
	{
	    LOG.error(e);
	    model.addAttribute("errorMessage", e.getMessage());
	    return ERROR;
	}
    }

    @RequestMapping(value = "/delete", method = RequestMethod.GET)
    public String deleteFile(@RequestParam("fileName") String fileName,
	    @RequestParam("currentFolder") String currentURL, ModelMap model, HttpSession session)
    {
	try
	{
	    String fullPath = DISK + currentURL + "/" + fileName;
	    fileService.deleteFile(fullPath);

	    model.addAttribute("mutualList", fileService.getFromFolder(DISK + currentURL));
	    model.addAttribute("currentURL", currentURL);
	    return EXPLORER;
	}
	catch (Exception e)
	{
	    LOG.error(e);
	    model.addAttribute("errorMessage", e.getMessage());
	    return ERROR;
	}
    }
}
