package com.epam.maskaliou.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.logout.SecurityContextLogoutHandler;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class SecurityController
{
    public static final Logger LOG = Logger.getRootLogger();

    private static final String LOGIN = "login";
    private static final String ACCESS_DENIED = "accessDenied";
    private static final String LOGOUT = "redirect:/login?logout";

    @RequestMapping(value = "/login", method = RequestMethod.GET)
    public String login(ModelMap model, HttpSession session)
    {
	try
	{
	    Authentication auth = SecurityContextHolder.getContext().getAuthentication();
	    String name = auth.getName();

	    session.setAttribute("currentUser", name);
	    return LOGIN;
	}
	catch(Exception e)
	{
	    session.setAttribute("currentUser", "");
	    return LOGIN;
	}
    }

    @RequestMapping(value = "/accessDenied", method = RequestMethod.GET)
    public String accessDenied(ModelMap model, HttpSession session)
    {
	return ACCESS_DENIED;
    }

    @RequestMapping(value = "/logout", method = RequestMethod.GET)
    public String logoutPage(HttpServletRequest request, HttpServletResponse response, HttpSession session)
    {
	try
	{
	    Authentication auth = SecurityContextHolder.getContext().getAuthentication();
	    if (auth != null)
	    {
		new SecurityContextLogoutHandler().logout(request, response, auth);
	    }
	    return LOGOUT;
	}
	catch(Exception e)
	{
	    return LOGOUT;
	}
    }
}
