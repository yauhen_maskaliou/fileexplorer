package com.epam.maskaliou.service.impl;

import org.springframework.beans.factory.annotation.Autowired;

import com.epam.maskaliou.dao.IFileDAO;
import com.epam.maskaliou.entity.MutualList;
import com.epam.maskaliou.exception.CustomException;
import com.epam.maskaliou.service.IFileService;

public class FileServiceImpl implements IFileService
{
    private IFileDAO fileDAO;

    @Autowired
    public FileServiceImpl(IFileDAO fileDAO)
    {
	super();
	this.fileDAO = fileDAO;
    }

    @Override
    public MutualList getFromFolder(String url) throws CustomException
    {
	try
	{
	    return fileDAO.getFromFolder(url);
	}
	catch (CustomException e)
	{
	    LOG.error("Can not get from folder.");
	    throw new CustomException(e.getMessage());
	}
    }
    
    @Override
    public String getFileText(String url) throws CustomException
    {
	try
	{
	    return fileDAO.getFileText(url);
	}
	catch (CustomException e)
	{
	    LOG.error("Can't get file text.");
	    throw new CustomException(e.getMessage());
	}
    }

    @Override
    public void addNewFile(String path, String fileText) throws CustomException
    {
	try
	{
	    fileDAO.addNewFile(path, fileText);
	}
	catch (CustomException e)
	{
	    LOG.error("Can't add new file.");
	    throw new CustomException(e.getMessage());
	}

    }

    @Override
    public void addNewFolder(String path) throws CustomException
    {
	try
	{
	    fileDAO.addNewFolder(path);
	}
	catch (CustomException e)
	{
	    LOG.error("Can't add new folder.");
	    throw new CustomException(e.getMessage());
	}
    }

    @Override
    public void deleteFile(String path) throws CustomException
    {
	try
	{
	    fileDAO.deleteFile(path);
	}
	catch (CustomException e)
	{
	    LOG.error("Can't delete file.");
	    throw new CustomException(e.getMessage());
	}
    }

}
