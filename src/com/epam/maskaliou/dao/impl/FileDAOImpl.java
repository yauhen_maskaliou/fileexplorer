package com.epam.maskaliou.dao.impl;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

import com.epam.maskaliou.dao.IFileDAO;
import com.epam.maskaliou.entity.MutualList;
import com.epam.maskaliou.exception.CustomException;

public class FileDAOImpl implements IFileDAO
{
    @Override
    public MutualList getFromFolder(String url) throws CustomException
    {
	try
	{
	    File folder = new File(url);
	    List<File> folderList = new ArrayList<File>();
	    List<File> fileList = new ArrayList<File>();
	    
	    for (File f : folder.listFiles())
	    {
		if (f.isDirectory())
		{
		    folderList.add(f);
		}
		else
		{
		    fileList.add(f);
		}
	    }
	    return new MutualList(folderList, fileList);
	}
	catch (Exception e)
	{
	    LOG.error("Can't get from folder (DAO).");
	    throw new CustomException(e.getMessage());
	}
    }
    
    @Override
    public String getFileText(String url) throws CustomException
    {
	try (BufferedReader br = new BufferedReader(new FileReader(url)))
	{
	    StringBuilder sb = new StringBuilder();
	    String line = br.readLine();

	    while (line != null)
	    {
		sb.append(line);
		sb.append(System.lineSeparator());
		line = br.readLine();
	    }
	    String everything = sb.toString();
	    return everything;
	}
	catch (IOException e)
	{
	    LOG.error("Can't get file text (DAO).");
	    throw new CustomException(e.getMessage());
	}
    }

    @Override
    public void addNewFile(String path, String fileText) throws CustomException
    {
	try (FileWriter writer = new FileWriter(path, false))
	{
	    writer.write(fileText);
	    writer.flush();
	}
	catch (IOException e)
	{
	    LOG.error("Can't add new file (DAO).");
	    throw new CustomException(e.getMessage());
	}
    }
    
    @Override
    public void addNewFolder(String path) throws CustomException
    {
	try
	{
	    Path p = Paths.get(path);
	    Files.createDirectories(p);
	}
	catch (IOException e)
	{
	    LOG.error("Can't add new folder (DAO).");
	    throw new CustomException(e.getMessage());
	}
    }

    @Override
    public void deleteFile(String path) throws CustomException
    {
	try
	{
	    if(!new File(path).delete())
	    {
		throw new CustomException();
	    }
	}
	catch (Exception e)
	{
	    LOG.error("Can't delete file (DAO).");
	    throw new CustomException(e.getMessage());
	}
    }
}
