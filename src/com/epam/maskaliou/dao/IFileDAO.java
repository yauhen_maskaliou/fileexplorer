package com.epam.maskaliou.dao;

import org.apache.log4j.Logger;

import com.epam.maskaliou.entity.MutualList;
import com.epam.maskaliou.exception.CustomException;

public interface IFileDAO
{
    public static final Logger LOG = Logger.getRootLogger();

    public MutualList getFromFolder(String url) throws CustomException;

    public String getFileText(String url) throws CustomException;

    public void addNewFile(String path, String fileText) throws CustomException;

    public void addNewFolder(String path) throws CustomException;

    public void deleteFile(String path) throws CustomException;
}
