package com.epam.maskaliou.entity;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class MutualList
{
    private List<File> folderList;
    private List<File> fileList;

    public List<File> getFolderList()
    {
	return folderList;
    }

    public void setFolderList(List<File> folderList)
    {
	this.folderList = folderList;
    }

    public List<File> getFileList()
    {
	return fileList;
    }

    public void setFileList(List<File> fileList)
    {
	this.fileList = fileList;
    }

    public MutualList(List<File> folderList, List<File> fileList)
    {
	super();
	this.folderList = folderList;
	this.fileList = fileList;
    }

    public MutualList()
    {
	folderList = new ArrayList<File>();
	fileList = new ArrayList<File>();
    }

    @Override
    public int hashCode()
    {
	final int prime = 31;
	int result = 1;
	result = prime * result + ((fileList == null) ? 0 : fileList.hashCode());
	result = prime * result + ((folderList == null) ? 0 : folderList.hashCode());
	return result;
    }

    @Override
    public boolean equals(Object obj)
    {
	if (this == obj)
	    return true;
	if (obj == null)
	    return false;
	if (getClass() != obj.getClass())
	    return false;
	MutualList other = (MutualList) obj;
	if (fileList == null)
	{
	    if (other.fileList != null)
		return false;
	}
	else if (!fileList.equals(other.fileList))
	    return false;
	if (folderList == null)
	{
	    if (other.folderList != null)
		return false;
	}
	else if (!folderList.equals(other.folderList))
	    return false;
	return true;
    }
}
