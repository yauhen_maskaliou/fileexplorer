<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<c:url var="resources" value="/resources/theme1" scope="request" />

<html>
<head>
<link href="${resources}/css/core.css" rel="stylesheet">
<link rel="stylesheet" href="resources/css/bootstrap.min.css" />

</head>
<body>

	<div class="panel panel-default">
		<div class="panel-heading">
			<jsp:include page="header.jsp" />
			<h1 class="page-header"> ${fileName} </h1>
		</div>
		<div class="panel-body">
		
			${fileText}
			
		</div>
	</div>
	
	<form action="<c:url value="/back" />" style="display: inline-block;">
		<input name="currentFolder" type="hidden" value="${currentURL}" />
		<img src="<c:url value="/resources/images/back.jpg" />" >
		<button class="btn btn-link" style="text-decoration: none;" type="submit">
			Back
		</button>
	</form>
	
</body>
</html>