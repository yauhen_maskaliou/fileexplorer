<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<c:url var="resources" value="/resources/theme1" scope="request" />

<html>
<head>
<link href="<c:url value="/resources/css/bootstrap.min.css" />" rel="stylesheet">

<link href="<c:url value="/resources/css/simplePagination.css" />" rel="stylesheet">

<script type="text/javascript" src="<c:url value="/resources/js/jquery.min.js" />"></script>
<script type="text/javascript" src="<c:url value="/resources/js/jquery.simplePagination.js" />"></script>
<script type="text/javascript" src="<c:url value="/resources/js/script.js" />"></script>

</head>
<body>

	<div class="panel panel-default">
		<div class="panel-heading">
		<jsp:include page="header.jsp" />
		</div>
		
		<div class="panel-body">
		
			<form action="<c:url value="/admin/newFile" />" style="display: inline-block;">
				<input name="currentFolder" type="hidden" value="${currentURL}" />
				<img src="<c:url value="/resources/images/plus.jpg" />" >
				<button class="btn btn-link" style="text-decoration: none;" type="submit">
					New file
				</button>
			</form>

			
			<img src="<c:url value="/resources/images/empty.jpg" />" >
			<img src="<c:url value="/resources/images/empty.jpg" />" >
			
			<img src="<c:url value="/resources/images/plus.jpg" />" 
			
			<form></form>
			
			<form action="<c:url value="/admin/addFolder" />" style="display: inline-block;">
				<button type="submit" class="btn btn-link"> New Folder </button>
				<input name="folderName" type="text" required="required" pattern="[A-Za-z0-9]{1,20}" />
				<input name="currentFolder" type="hidden" value="${currentURL}" />
			</form>
			<br /> <br />

			Current directory: D:/${currentURL}
			<div style="height:300;">
			<table id="files">
			<tbody>
			<c:forEach items="${mutualList.folderList}" var="folder"> 
				<tr> 
					<td> 
						<form action="<c:url value="/viewFolder" />" >
							<input name="folderName" type="hidden" value="${folder.name}" />
							<input name="currentFolder" type="hidden" value="${currentURL}" />
							<img src="<c:url value="/resources/images/folder.jpg" />" >
							<button type="submit" class="btn btn-link"> ${folder.name} </button>
						</form>
					</td>
						
					<td>
						<form action="<c:url value="/admin/delete" />" style="display: inline-block;">
							<input name="fileName" type="hidden" value="${folder.name}" />
							<input name="currentFolder" type="hidden" value="${currentURL}" />
							<button class="btn btn-link" style="text-decoration: none;" type="submit">
								<img src="<c:url value="/resources/images/delete.jpg" />" >
							</button>
						</form>
					</td>
				</tr>
			</c:forEach>

			<c:forEach items="${mutualList.fileList}" var="file"> 
				<tr> 
					<td> 
						<form action="<c:url value="/viewFile" />" style="display: inline-block;">
							<input name="fileName" type="hidden" value="${file.name}" />
							<input name="currentFolder" type="hidden" value="${currentURL}" />
							<img src="<c:url value="/resources/images/file.jpg" />" >
							<button type="submit" class="btn btn-link"> ${file.name} </button>
						</form>
					</td>
		
					<td>
						<form action="<c:url value="/admin/delete" />" style="display: inline-block;">
							<input name="fileName" type="hidden" value="${file.name}" />
							<input name="currentFolder" type="hidden" value="${currentURL}" />
							<button class="btn btn-link" style="text-decoration: none;" type="submit">
								<img src="<c:url value="/resources/images/delete.jpg" />" >
							</button>
						</form>
					</td>
				</tr>
			</c:forEach>
			
			</tbody> 
			</table>
			</div>
			<br />
			
			<div id="pagination" class="pagination" align="center"> </div>
			
		</div>
	</div>
	
	<div class="panel-body">
		<form action="<c:url value="/back" />" style="display: inline-block;">
			<input name="currentFolder" type="hidden" value="${currentURL}" />
			<img src="<c:url value="/resources/images/back.jpg" />" >
			<button class="btn btn-link" style="text-decoration: none;" type="submit">
				Back
			</button>
		</form>
	</div>
	
</body>
</html>