<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<html>
<head>

<link href="<c:url value="/resources/css/bootstrap.min.css" />" rel="stylesheet">

</head>
<body>

	<form action="addFile">
		<div class="panel panel-default">
			<div class="panel-heading">
				<jsp:include page="header.jsp" />
				File Name: <br />
				<textarea maxlength="50" rows="2" cols="80" name="fileName"> </textarea>
			</div>
			
			<div class="panel-body">
				File Text: <br />
				<textarea maxlength="5000" rows="17" cols="80" name="fileText"></textarea> <br />
				
				<button type="submit" class="btn btn-success"> Add file </button>
			</div>
		</div>
		
		<div class="panel-body">
			<form action="<c:url value="/back" />" style="display: inline-block;">
				<input name="currentFolder" type="hidden" value="${currentURL}" />
				<img src="<c:url value="/resources/images/back.jpg" />" >
				<button class="btn btn-link" style="text-decoration: none;" type="submit">
					Back
				</button>
			</form>
		</div>
		
	</form>
	
</body>
</html>