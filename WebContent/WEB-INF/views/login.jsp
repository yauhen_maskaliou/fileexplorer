<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html>
  <head>
    <title tiles:fragment="title">Messages : Create</title>
  </head>
  <link href="<c:url value="/resources/css/bootstrap.min.css" />" rel="stylesheet">
  <body>
  
  
  <div class="panel panel-default">
		<div class="panel-heading">
			<h1 class="page-header"> Log in </h1>
		</div>
		
		
		<form action="<c:url value="/login" />" method="post">
		<input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
		<div class="panel-body">
			 <label for="username">Username</label>
             <input type="text" id="username" name="username" class="form-control" style="width: 300" />
             <br />        
             <label for="password">Password</label>
             <input type="password" id="password" name="password" class="form-control" style="width: 300" />
             <br /> <br />
             <div class="form-actions">
             	<button type="submit" class="btn btn-success btn-lg" style="width: 300" >Log in</button>
             </div>
		</div>
		
		</form>
		
	</div>
  </body>
</html>